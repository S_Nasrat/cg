#Computational Geometry Library#

This is a Computational Geometry Library implemented mainly to be used in educational purposes.

### What is this repository for? ###

* Implementing a well-designed, documented Computational Geometry(CG) library to be used in educational purposes that enables students to spend their time designing a good CG algorithms rather than designing the library itself and helps the TA's also in testing the students' purposed solutions efficiently.


### Currently Working In ###

* Set the basic structure and design of the library 

### To-Do feature list ###
* 
### Participants ###
- Anas Awad (eng.anas.awad@gmail.com).
- Bassel Safwat (kasparov092@gmail.com).
- Muhammad Abbady (aladly.forever@gmail.com).
- Taha Mahmoud (tahamahmoud92@gmail.com).

### Repo admin ###
* Mohammad Sharaf (mohammedsharaf.1992@gmail.com).