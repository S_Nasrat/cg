﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class GrahamScan : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            int minX, minY, maxX, maxY;
            //Get the minY
            //Get all angles from the line minY , minY.X+1 and all the other points
            //Sort them in ascending order
            //remove minY from points
            HelperMethods.GetExtremes(points, out minX, out maxX, out minY, out maxY);
            Point p1 = new Point(points[minY].X, points[minY].Y);
            Point p2 = new Point(p1.X,p1.Y);
            p2.X++;
            outPoints.Add(p1);
            points.RemoveAt(minY);
            points.Sort((mP1, mP2) => HelperMethods.VectorAngle(p1, p2, mP1).CompareTo(HelperMethods.VectorAngle(p1, p2, mP2)));
            outPoints.Add(points[0]);
            points.RemoveAt(0);
            for (int i = 0; i < points.Count;)
            {
                if (HelperMethods.CheckTurn(new Line(outPoints[outPoints.Count - 2], outPoints[outPoints.Count - 1]), points[i]) == Enums.TurnType.Left)
                {
                    outPoints.Add(points[i]);
                    i++;
                }
                else
                {
                    outPoints.RemoveAt(outPoints.Count - 1);
                }
            }

        }
        public int ComparerFunction(Point A, Point B,Point p1, Point p2)
        {
            double angleA = HelperMethods.VectorAngle(p1, p2, A);
            double angleB = HelperMethods.VectorAngle(p1, p2, B);
            if (angleA > angleB)
            {
                return 1;
            }
            else if (angleA < angleB)
            {
                return -1;
            }
            else
                return 0;
        }

        public override string ToString()
        {
            return "Convex Hull - Graham Scan";
        }
    }
}
