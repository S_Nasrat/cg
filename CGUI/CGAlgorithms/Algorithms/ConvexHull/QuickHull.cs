﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class QuickHull : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            // Initialization
            int minXI, maxXI, minYI, maxYI;
            HelperMethods.GetExtremes(points, out minXI, out maxXI,out  minYI, out maxYI);
            
            List<Point> RHP = new List<Point>();
            List<Point> LHP = new List<Point>();
            //Getting points on the left chain and right chain
            for (int i = 0; i < points.Count; i++)
            {
                if (i != minXI && i != maxXI)
                {
                    //Checking turn right
                    if (HelperMethods.CheckTurn(new Line(points[minXI], points[maxXI]), points[i]) == Enums.TurnType.Right)
                    {
                        RHP.Add(points[i]);
                    }
                    else if (HelperMethods.CheckTurn(new Line(points[minXI], points[maxXI]), points[i]) == Enums.TurnType.Left)
                    {
                        LHP.Add(points[i]);
                    }
                }
            }
            List<Point> l1 = new List<Point>(QH(points[minXI], points[maxXI], LHP));
            l1.Add(points[minXI]);
            List<Point> l2 = new List<Point>(QH(points[maxXI], points[minXI], RHP));
            l2.Add(points[maxXI]);
            outPoints = l1.Concat(l2).ToList();
          
            outPoints = outPoints.Distinct().ToList();


        }
        

        public override string ToString()
        {
            return "Convex Hull - Quick Hull";
        }
        public List<Point> QH(Point min, Point max, List<Point> SomePs)
        {
            List<Point> returnPoints = new List<Point>();
            if (SomePs.Count > 0)
            {
                Point maxPoint = SomePs[HelperMethods.GetMaxDistancePointFromLine(min, max, SomePs)];
                List<Point> NW = new List<Point>(HelperMethods.GetOutsidePoints(min, maxPoint, SomePs));
                List<Point> NE = new List<Point>(HelperMethods.GetOutsidePoints(maxPoint, max, SomePs));
                returnPoints = new List<Point>(QH(min, maxPoint, NW));
                returnPoints.Add(maxPoint);
                returnPoints = returnPoints.Concat(QH(maxPoint,max,NE)).ToList();
            }
            return returnPoints;
        }
    }
}
