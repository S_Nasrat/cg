﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremeSegments : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
           
            bool inLeftPlane;

            for (int i = 0; i < points.Count; i++)
            {
                for (int j = 0; j < points.Count; j++)
                {
                    inLeftPlane = true;
                    if (i != j)
                    {
                        Line extremeSegment = new Line(points[i], points[j]);
                        for (int k = 0; k < points.Count; k++)
                        {
                            if (i != k && j != k)
                            {
                                if (HelperMethods.CheckTurn(extremeSegment, points[k]) == Enums.TurnType.Right || (HelperMethods.CheckTurn(extremeSegment, points[k]) == Enums.TurnType.Colinear && !HelperMethods.PointOnSegment(points[k],extremeSegment.Start,extremeSegment.End)))
                                {
                                    inLeftPlane = false;
                                    break;
                                }
                            }

                        }
                        if (inLeftPlane == true)
                        {
                            outPoints.Add(extremeSegment.Start);
                            outPoints.Add(extremeSegment.End);
                        }
                    }
                }
            }
           outPoints = outPoints.Distinct().ToList();
        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Segments";
        }
    }
}
