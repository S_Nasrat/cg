﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class JarvisMarch : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {

            Point minY = new Point(0, double.MaxValue);
            int minYIndex = 0;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].Y < minY.Y)
                {
                    minY = points[i];
                    minYIndex = i;

                }
            }
            outPoints.Add(minY);
          //  points.Remove(minY);
            //1st Iteration
            double minAngle = double.MaxValue;
            int currIndex = 0;
            for (int i = 0; i < points.Count; i++)
            {
                if (i != minYIndex)
                {
                    double tempAngle = HelperMethods.AngleBetweenTwoVectors(points[i], points[minYIndex], new Point(minY.X + 1, minY.Y));
                    if (tempAngle < minAngle)
                    {
                        minAngle = tempAngle;
                        
                        currIndex = i;
                    }
                }
               
            }
            outPoints.Add(points[currIndex]);
          //  points.Remove(points[currIndex]);
            do
            {
                minAngle = double.MaxValue;
                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i].Equals(outPoints[outPoints.Count-1]) == false)
                    {
                        double tempAngle = HelperMethods.AngleBetweenTwoVectorsReversed(points[i], outPoints[outPoints.Count-1], outPoints[outPoints.Count - 2]);
                        if (tempAngle < minAngle)
                        {
                            minAngle = tempAngle;
                         
                            currIndex = i;
                        }
                    }
                   
                }
                if (points[currIndex].Equals(outPoints[0]) || minAngle == 0)
                    break;
                outPoints.Add(points[currIndex]);
              //  points.Remove(points[currIndex]);
            }
           while(true);
            outPoints = outPoints.Distinct().ToList();
        }

        public override string ToString()
        {
            return "Convex Hull - Jarvis March";
        }
    }
}
