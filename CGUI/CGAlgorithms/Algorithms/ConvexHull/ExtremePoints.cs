﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremePoints : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            // I have made a modification in this file..  
            //Replace j = i+1 with j =0 and the rest of the loops but time increases.
            outPoints = new List<Point>(points);
            for (int i = 0; i < points.Count; i++)
            {
                for (int j = 0; j < points.Count; j++)
                {
                    for (int k = 0; k < points.Count; k++)
                    {
                        for (int l = 0; l < points.Count; l++)
                        {
                            if (points[i] != points[j] && points[i] != points[k] && points[i] != points[l])
                            {
                                if (HelperMethods.PointInTriangle(points[i], points[j], points[k], points[l]) == Enums.PointInPolygon.Inside || HelperMethods.PointInTriangle(points[i],points[j],points[k],points[l])==Enums.PointInPolygon.OnEdge)
                                {
                                    outPoints.Remove(points[i]);
                                    
                                }
                            }
                        }
                    }
                }
            }
        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Points";
        }
    }
}
