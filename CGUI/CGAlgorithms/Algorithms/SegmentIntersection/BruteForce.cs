﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.SegmentIntersection 
{
    class BruteForce : Algorithm
    {
         public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            foreach (Line l in lines)
            {
                foreach (Line m in lines)
                {
                     //if l intersects m
                     //Do something
                    Point temp = GetPointofIntersection(l, m);
                   
                        outPoints.Add(temp);
                    

                 }
             }
        }

        public override string ToString()
        {
            return "Brute Force Solution";
        }
        public double GetSlope(Point S, Point E)
        {
            return (E.Y - S.Y) / (E.X - S.X);
        }
        public double GetB(Point P,double a)
        {
            return (P.Y) - (a * P.X);
        }
        public Point GetPointofIntersection(Line L1,Line L2)
        {
            double slopeL1 = GetSlope(L1.Start, L2.End);
            double slopeL2 = GetSlope(L2.Start, L2.End);

            double b1 = GetB(L1.Start, slopeL1);
            double b2 = GetB(L2.Start, slopeL2);

            double X = (b2 - b1) / (slopeL1 - slopeL2);
            double y1 = (X * slopeL1)+ b1;
            double y2 = (X * slopeL2) + b2;
            if (y1 == y2)
            {
                Point intersectionPoint = new Point(X, y1);
                if (HelperMethods.PointOnSegment(intersectionPoint, L1.Start, L1.End) && HelperMethods.PointOnSegment(intersectionPoint, L2.Start, L2.End))
                {
                    return intersectionPoint;
                }
                else
                    return new Point(0, 0);

            }
            else
               
                return new Point(0, 0);

            
        }
    }
}
