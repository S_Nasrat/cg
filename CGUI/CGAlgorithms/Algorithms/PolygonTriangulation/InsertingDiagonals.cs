﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class InsertingDiagonals : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
           
            List<Line> outdiagonals = new List<Line>();
            InsertDiagonals(polygons[0], ref outdiagonals);
            outLines = new List<Line>(outdiagonals);
        }

        public override string ToString()
        {
            return "Inserting Diagonals";
        }
        public void InsertDiagonals(Polygon p,ref List<Line> diag)
        {
            if (p.lines.Count > 3)
            {
                int minP = HelperMethods.GetMinPointIndex(p);
                int maxDistance = HelperMethods.GetMaxDistance(p, minP);
                int startDiag=0, endDiag=0;
                if (maxDistance == -1)
                {
                    diag.Add(new Line(HelperMethods.PreviousPoint(p, minP), HelperMethods.NextPoint(p, minP)));            
                  Point start = HelperMethods.NextPoint(p, minP,ref startDiag);
                  Point end = HelperMethods.PreviousPoint(p, minP,ref endDiag);
                }
                else
                {
                    diag.Add(new Line(p.lines[minP].Start, p.lines[maxDistance].Start));
                    startDiag = maxDistance;
                    endDiag = minP;
                }

                Polygon p1, p2;
                p1 = HelperMethods.SubDiagonalNext(p, endDiag ,startDiag);
                p2 = HelperMethods.SubDiagonalNext(p, startDiag, endDiag);

                InsertDiagonals(p1,ref diag);
                InsertDiagonals(p2,ref diag);
 
            }

        }
    }
}
