﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGUtilities
{
    public class SmartPoint
    {
        public Point mPoint;
        public Chain_Type mType;
        public enum Chain_Type
        {
            Left,Right
        };
        public SmartPoint()
        {
            
        }
        public SmartPoint(Point p, Chain_Type t)
        {
            mPoint = p;
            mType = t;
        }
        
    }
}
