﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGUtilities
{
    public class HelperMethods
    {
        public static Enums.PointInPolygon PointInTriangle(Point p, Point a, Point b, Point c)
        {
            if (a.Equals(b) && b.Equals(c))
            {
                if (p.Equals(a) || p.Equals(b) || p.Equals(c))
                    return Enums.PointInPolygon.OnEdge;
                else
                    return Enums.PointInPolygon.Outside;
            }

            Line ab = new Line(a, b);
            Line bc = new Line(b, c);
            Line ca = new Line(c, a);

            if (GetVector(ab).Equals(Point.Identity)) return (PointOnSegment(p, ca.Start, ca.End)) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;
            if (GetVector(bc).Equals(Point.Identity)) return (PointOnSegment(p, ca.Start, ca.End)) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;
            if (GetVector(ca).Equals(Point.Identity)) return (PointOnSegment(p, ab.Start, ab.End)) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;

            if (CheckTurn(ab, p) == Enums.TurnType.Colinear)
                return PointOnSegment(p, a, b) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;
            if (CheckTurn(bc, p) == Enums.TurnType.Colinear && PointOnSegment(p, b, c))
                return PointOnSegment(p, b, c) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;
            if (CheckTurn(ca, p) == Enums.TurnType.Colinear && PointOnSegment(p, c, a))
                return PointOnSegment(p, a, c) ? Enums.PointInPolygon.OnEdge : Enums.PointInPolygon.Outside;

            if (CheckTurn(ab, p) == CheckTurn(bc, p) && CheckTurn(bc, p) == CheckTurn(ca, p))
                return Enums.PointInPolygon.Inside;
            return Enums.PointInPolygon.Outside;
        }

        public static Enums.TurnType CheckTurn(Point vector1, Point vector2)
        {
            double result = CrossProduct(vector1, vector2);
            if (result < 0) return Enums.TurnType.Right;
            else if (result > 0) return Enums.TurnType.Left;
            else return Enums.TurnType.Colinear;
        }
        public static double CrossProduct(Point a, Point b)
        {
            return a.X * b.Y - a.Y * b.X;
        }
        public static double DotProduct(Point a, Point b)
        {
            return a.X * b.X + a.Y * b.Y;
        }
        public static double AngleBetweenTwoVectors(Point Pup, Point Pmid, Point Pdown)
        {
            Point Vector1 = new Point(Pup.X - Pmid.X, Pup.Y - Pmid.Y);
            Point Vector2 = new Point(Pdown.X - Pmid.X, Pdown.Y - Pmid.Y);


            double crossP = CrossProduct(Vector1, Vector2);
            double dProduct = DotProduct(Vector1, Vector2);

            double angle = Math.Atan2(crossP, dProduct) * 180 / Math.PI;
            if (angle < 0)
            {
                angle += 360;
            }
            return angle;
        }
        public static double AngleBetweenTwoVectorsReversed(Point Pup, Point Pmid, Point Pdown)
        {
            Point Vector1 = new Point(Pup.X - Pmid.X, Pup.Y - Pmid.Y);
            Point Vector2 = new Point(Pmid.X - Pdown.X, Pmid.Y - Pdown.Y);


            double crossP = CrossProduct(Vector1, Vector2);
            double dProduct = DotProduct(Vector1, Vector2);

            double angle = Math.Atan2(crossP, dProduct) * 180 / Math.PI;
            if (angle < 0)
            {
                angle += 360;
            }
            return angle;
        }
        public static bool PointOnRay(Point p, Point a, Point b)
        {
            if (a.Equals(b)) return true;
            if (a.Equals(p)) return true;
            var q = a.Vector(p).Normalize();
            var w = a.Vector(b).Normalize();
            return q.Equals(w);
        }
        public static bool PointOnSegment(Point p, Point a, Point b)
        {
            if (a.Equals(b))
                return p.Equals(a);

            if (b.X == a.X)
                return p.X == a.X && (p.Y >= Math.Min(a.Y, b.Y) && p.Y <= Math.Max(a.Y, b.Y));
            if (b.Y == a.Y)
                return p.Y == a.Y && (p.X >= Math.Min(a.X, b.X) && p.X <= Math.Max(a.X, b.X));
            double tx = (p.X - a.X) / (b.X - a.X);
            double ty = (p.Y - a.Y) / (b.Y - a.Y);

            return (Math.Abs(tx - ty) <= Constants.Epsilon && tx <= 1 && tx >= 0);
        }
        /// <summary>
        /// Get turn type from cross product between two vectors (l.start -> l.end) and (l.end -> p)
        /// </summary>
        /// <param name="l"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static Enums.TurnType CheckTurn(Line l, Point p)
        {
            Point a = l.Start.Vector(l.End);
            Point b = l.End.Vector(p);
            return HelperMethods.CheckTurn(a, b);
        }
        public static Point GetVector(Line l)
        {
            return l.Start.Vector(l.End);
        }
        public static double GetDistanceFromPointToLine(Point p, Point Start, Point End)
        {
            double distance = (Math.Abs((End.Y - Start.Y) * p.X - (End.X - Start.X) * p.Y + End.X * Start.Y - End.Y * Start.X));
            distance /= Math.Sqrt(Math.Pow((End.Y - Start.Y), 2) + Math.Pow((End.X - Start.X), 2));
            return distance;
        }
        public static void GetExtremes(List<Point> L, out int minXIndex, out int maxXIndex, out int minYIndex, out int maxYIndex)
        {
            minXIndex = 0;
            maxXIndex = 0;
            minYIndex = 0;
            maxYIndex = 0;
            double tempXMin = double.MaxValue;
            for (int i = 0; i < L.Count; i++)
            {
                if (L[i].X < tempXMin)
                {
                    tempXMin = L[i].X;
                    minXIndex = i;
                }
            }
            double tempXMax = double.MinValue;
            for (int i = 0; i < L.Count; i++)
            {
                if (L[i].X > tempXMax)
                {
                    tempXMax = L[i].X;
                    maxXIndex = i;
                }
            }
            double tempYMin = double.MaxValue;
            for (int i = 0; i < L.Count; i++)
            {
                if (L[i].Y < tempYMin)
                {
                    tempYMin = L[i].Y;
                    minYIndex = i;
                }
            }
            double tempYMax = double.MinValue;
            for (int i = 0; i < L.Count; i++)
            {
                if (L[i].Y > tempYMax)
                {
                    tempYMax = L[i].Y;
                    maxYIndex = i;
                }
            }
        }
        public static int GetMinPointIndex(Polygon p)
        {
            int minPIndex = 0;
            Point minPoint = new Point(double.MaxValue, double.MaxValue);
            for (int i = 0; i < p.lines.Count; i++)
            {
                if (p.lines[i].Start.X < minPoint.X)
                {
                    minPoint.X = p.lines[i].Start.X;
                    minPoint.Y = p.lines[i].Start.Y;
                    minPIndex = i;

                }
                else if (p.lines[i].Start.X == minPoint.X)
                {
                    if (p.lines[i].Start.Y < minPoint.Y)
                    {
                        minPoint.X = p.lines[i].Start.X;
                        minPoint.Y = p.lines[i].Start.Y;
                        minPIndex = i;

                    }
                }
            }
            return minPIndex;

        }
        public static bool IsConvexPoint(Point prevA, Point A, Point nextA)
        {
            if (HelperMethods.CheckTurn(new Line(prevA, A), nextA) == Enums.TurnType.Left)
            {
                return true;
            }
            else
                return false;
        }
        public static Point PreviousPoint(Polygon p, int pointIndex)
        {
            if (pointIndex == 0)
            {
                return p.lines[p.lines.Count - 1].Start;
            }
            else
            {
                return p.lines[pointIndex - 1].Start;
            }
        }
        public static Point PreviousPoint(Polygon p, int pointIndex,ref int prevPointIndex)
        {
            if (pointIndex == 0)
            {
                prevPointIndex = p.lines.Count - 1;
                return p.lines[p.lines.Count - 1].Start;
            }
            else
            {
                prevPointIndex = pointIndex - 1;
                return p.lines[pointIndex - 1].Start;
            }
        }
        public static Point NextPoint(Polygon p, int pointIndex)
        {
            if (pointIndex == p.lines.Count - 1)
            {
                return p.lines[0].Start;
            }
            else
            {
                return p.lines[pointIndex + 1].Start;
            }
        }
        public static Point NextPoint(Polygon p, int pointIndex,ref int nextPointIndex)
        {
            if (pointIndex == p.lines.Count - 1)
            {
                nextPointIndex = 0;
                return p.lines[0].Start;
            }
            else
            {
                nextPointIndex = pointIndex + 1;
                return p.lines[pointIndex + 1].Start;
            }
        }

        public static bool isClockWise(Polygon p)
        {
            bool Clockwise = false;
            int minPointIndex = GetMinPointIndex(p);
            if (CheckTurn(new Line(PreviousPoint(p, minPointIndex), HelperMethods.NextPoint(p, minPointIndex)), p.lines[minPointIndex].Start) == Enums.TurnType.Left)
            {
                Clockwise = true;
            }
            else
                Clockwise = false;
            return Clockwise;
        }
        public static bool isEar(Polygon p, int pointIndex)
        {
            bool pointsInTriangle = false;
            if (IsConvexPoint(PreviousPoint(p, pointIndex), p.lines[pointIndex].Start, NextPoint(p, pointIndex)))
            {
                for (int i = 0; i < p.lines.Count; i++)
                {
                    if (PointInTriangle(p.lines[i].Start, PreviousPoint(p, pointIndex), p.lines[pointIndex].Start, NextPoint(p, pointIndex)) == Enums.PointInPolygon.Inside)
                    {
                        pointsInTriangle = true;
                        break;
                    }
                }
                if (!pointsInTriangle)
                {
                    return true;
                }
                else return false;
            }
            else
                return false;
        }
        public static int GetMaxDistance(Polygon p, int minP)
        {
            bool PointsInTriangle = false;
            List<int> psDis = new List<int>();
            for (int i = 0; i < p.lines.Count; i++)
            {
                if(PointInTriangle(p.lines[i].Start,PreviousPoint(p,minP),p.lines[minP].Start,NextPoint(p,minP))==Enums.PointInPolygon.Inside)
                {
                    PointsInTriangle = true;
                    psDis.Add(i);
                }
            }
            int maxDistancePIndex = 0;
            if (PointsInTriangle)
            {

                double maxDist = double.MinValue;
                for (int i = 0; i < psDis.Count; i++)
                {
                    double temp = GetDistanceFromPointToLine(p.lines[psDis[i]].Start, PreviousPoint(p, minP), NextPoint(p, minP));
                    if (temp > maxDist)
                    {
                        maxDistancePIndex = psDis[i] ;
                        maxDist = temp;
                    }
                }
            }
            else
                maxDistancePIndex = -1; //No point in Triangle
            
            return maxDistancePIndex;
        }

        public static Polygon SubDiagonalNext(Polygon p, int start, int end)
        {
            Polygon subPoly = new Polygon();
            Point sub_start = p.lines[start].Start;
            Point sub_end = p.lines[end].Start;
            Point sub_temp = sub_start;
            int sub_temp_index = start;
           while(!sub_temp.Equals(sub_end))
           {
               subPoly.lines.Add(new Line(sub_temp, NextPoint(p, sub_temp_index)));
               sub_temp = NextPoint(p, sub_temp_index);
          //     sub_temp_index = NextPointIndex(p, sub_temp_index); //updated the index
               if(sub_temp_index == p.lines.Count - 1)
               {
                   sub_temp_index = 0;
               }
               else
               {
                   sub_temp_index++;
               }
           }
            subPoly.lines.Add(new Line(p.lines[end].Start, p.lines[start].Start));
            return subPoly;
        }
        public static bool isConvexAngle3Points(Point start,Point end,SmartPoint p)
        {
            if (p.mType == SmartPoint.Chain_Type.Right)
            {
                if (CheckTurn(new Line(start, end), p.mPoint) == Enums.TurnType.Right)
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (CheckTurn(new Line(start, end), p.mPoint) == Enums.TurnType.Left)
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public static SmartPoint getPreOfTopStack(Stack<SmartPoint> sStack)
        {
            SmartPoint top = new SmartPoint();
            SmartPoint previous_of_Top = new SmartPoint();
            top = sStack.Pop();
            previous_of_Top = sStack.Pop();
            sStack.Push(previous_of_Top);
            sStack.Push(top);
            return previous_of_Top;

        }
        public static int getMinY(Polygon p)
        {
            Point minY = new Point(double.MaxValue, double.MaxValue);
            int indexofMinY = 0;
            for (int i = 0; i < p.lines.Count; i++)
            {
                if (p.lines[i].Start.Y > minY.Y)
                {
                    minY.Y = p.lines[i].Start.Y;
                    indexofMinY = i;
                }
                
            }
            return indexofMinY;
        }
        

    }
}
